import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Dimensions } from 'react-native';
import Images from '../../assets/images';

export default class ZodiacSign extends Component<Props> {

  render() {
  	const {sign, dates} = this.props;
  	const img = sign.toLowerCase();
    return (
    	<View style={styles.view}>
	      <TouchableOpacity style={styles.iconContainer} onPress={() => this.props.navigation.navigate('Horoscope', {sign: this.props.sign, dates: this.props.dates})}>
	      	<Image source={{uri: img}} 
	      		   style={styles.icon}
	      		   resizeMode="contain"/>	       	
	      </TouchableOpacity>
	      <Text style={styles.name}>{sign}</Text>
	    </View>
    );
  }
}

const screenWidth = Dimensions.get('window').width;
const viewWidth = (1/3 * screenWidth - (1 - 1/3) * 20);

const styles = StyleSheet.create({
  view: {
  	width: viewWidth,
  	marginBottom: 20,
  },
  iconContainer: {
  	width: viewWidth,
  	height: viewWidth,
  	borderWidth: 2,
  	borderRadius: viewWidth/2,
  	borderColor: '#A5C3D3',
  	justifyContent: 'center',
  	alignItems: 'center',

  },
  dates: {
  	fontSize: 16,
    textAlign: 'center',
    color: '#A5C3D3'
  },
  name: {
  	fontSize: 16,
    textAlign: 'center',
    color: '#A5C3D3',
    fontWeight: 'bold'
  },
  icon: {
  	width: 80,
  	height: 80,
  }
});