import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ScrollView, ImageBackground } from 'react-native';
import { HeaderBackButton } from 'react-navigation';
import { format } from 'date-fns';

export default class Horoscope extends Component<Props> {

  constructor(props) {
    super(props);

    this.state = {
      horoscopeData: null
    };

  }
  componentWillMount() {
    this.sign = this.props.navigation.getParam('sign', 'Aries')
    this.dates = this.props.navigation.getParam('dates', '')
  }

  componentDidMount() {
    fetch(`https://aztro.sameerkumar.website?sign=${this.sign.toLowerCase()}&day=today`, {
      method: "POST",
      headers: {
      "Content-Type":'application/json'
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        // console.warn(responseJson)
        this.setState({
          horoscopeData: responseJson.description,
          luckyNumber: responseJson.lucky_number,
          mood: responseJson.mood,
          luckyTime: responseJson.lucky_time,
          color: responseJson.color,
          compatibility: responseJson.compatibility
        });
      });
  }

  static navigationOptions =  ({ navigation  }) =>{
    const sign = navigation.getParam('sign', 'Your Horoscope');
    return {
      title: sign,
      headerLeft: <HeaderBackButton onPress={() => navigation.goBack(null)} />,
      headerRight: null

    }
  }

  render() {
    const img = this.sign.toLowerCase();
    const currentDate = format(new Date(), 'dddd, MMMM D, YYYY')
    return (
      <ImageBackground source={{uri: 'fullbackground'}} style={styles.backgroundImage}>
        <View style={styles.body}>
          <View>
            <Image source={{uri: img}} 
                   style={styles.icon}
                   resizeMode="contain"/>  
            <View style={styles.headerText}>
              <Text style={styles.sign}>{this.sign}</Text>
              <Text style={styles.dates}>{this.dates}</Text>
            </View>
          </View>
          <Text style={styles.currentDate}>{currentDate}</Text>
          <Text style={styles.horoscope}>{this.state.horoscopeData}</Text>
          <Text style={styles.horoscope}>You're compatible with <Text style={{fontWeight: 'bold'}}>{this.state.compatibility}</Text></Text>
          <Text style={styles.horoscope}>Your lucky number is <Text style={{fontWeight: 'bold'}}>{this.state.luckyNumber}</Text></Text>
          <Text style={styles.horoscope}>Your lucky lucky time is <Text style={{fontWeight: 'bold'}}>{this.state.luckyTime}</Text></Text>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  body: {
    paddingLeft: 16,
    paddingRight: 16
  },
  backgroundImage: {
   height: '100%',
   width: '100%'
  },
  headerText: {
    position: 'absolute',
    top: 25,
    left: 150
  },
  sign: {
    color: '#FFFFFF',
    fontSize: 36
  },
  dates: {
    color: '#FFFFFF',
    fontSize: 20
  },
  icon: {
    width: 115,
    height: 115,
    marginTop: 15,
    marginLeft: 15
  },
  currentDate: {
    color: '#A5C4D4',
    fontSize: 20,
    lineHeight: 26
  },
  horoscope: {
    color: '#FFFFFF',
    fontSize: 20,
    lineHeight: 26,
    // fontFamily: 'Roboto',
    textAlign: 'justify',
    paddingTop: 10
  }
});