import React, { Component } from 'react';
import ZodiacSign from '../ZodiacSign';
import SignMap from './../../signMap'
import { format } from 'date-fns'
import { StyleSheet, Text, View, ImageBackground, ScrollView } from 'react-native';


export default class SignGallery extends Component<Props> {

  static navigationOptions =  ({ navigation  }) =>{
    const sign = navigation.getParam('sign', 'Your Horoscope');
    return {
      headerTitleStyle: {
        textAlign: 'center',
        width: '100%',
        fontSize: 26,
        fontWeight: '200'
      }
    }
  }

  render() {
    const currentDay = format(new Date(), 'D');
    const currentMonth = format(new Date(), 'MMMM');
    const currentYear = format(new Date(), 'YYYY');
    const { navigate } = this.props.navigation;
    return (
      <ScrollView>
        <View>
          <ImageBackground source={{uri: 'background'}} 
                 style={styles.header}> 
            <Text style={styles.day}>{currentDay}</Text>
            <Text style={styles.month}>{currentMonth}</Text>
            <Text style={styles.year}>{currentYear}</Text>
          </ImageBackground>
         
        </View>
        <View style={styles.gallery}>
          {
            SignMap.map(sign => <ZodiacSign key={sign.name} sign={sign.name} dates={sign.dates} navigation={this.props.navigation} onSelectSign={this.props.selectSign} />)
          }
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    height: 118
  },
  gallery: {
    display: 'flex',
    flexDirection: 'row',
    // flex: 1,
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    backgroundColor: '#230E14',
    width: '100%',
    margin: 'auto',
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10
  },
  day: {
    color: '#ffffff',
    fontSize: 90,
    position: 'absolute',
    top: 0,
    left: 15
  },
  month: {
    color: '#ffffff',
    fontSize: 36,
    position: 'absolute',
    top: 20,
    left: 140
  },
  year: {
    color: '#ffffff',
    fontSize: 24,
    position: 'absolute',
    top: 65,
    left: 140
  }

});


