import React, { Component } from 'react';
import { ScrollView, Text, View } from 'react-native';

class SideMenu extends Component {
  goToScreen = (route) => () => {
    this.props.navigation.navigate(route);
  }

  render () {
    return (
      <View>
        <ScrollView>
          <View>
            <View>
              <Text onPress={() => this.props.navigation.goBack(null)}>
                Home
              </Text>
            </View>
          </View>
          <View>
            <View>
              <Text onPress={this.goToScreen('Terms')}>
                Terms
              </Text>
              <Text onPress={this.goToScreen('Settings')}>
                Settings
              </Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default SideMenu;