const Images = {
	Aquarius: require('../assets/aquarius.svg'),
	Aries: require('../assets/aries.svg'),
	Cancer: require('../assets/cancer.svg'),
	Capricorn: require('../assets/capricorn.svg'),
	Gemini: require('../assets/gemini.svg'),
	Leo: require('../assets/leo.svg'),
	Libra: require('../assets/libra.svg'),
	Pisces: require('../assets/pisces.svg'),
	Sagittarius: require('../assets/sagittarius.svg'),
	Scorpio: require('../assets/scorpio.svg'),
	Taurus: require('../assets/taurus.svg'),
	Virgo: require('../assets/virgo.svg'),
	Aquarius: require('../assets/aquarius.svg'),
	Aries: require('../assets/aries.svg'),
	Cancer: require('../assets/cancer.svg'),
	background: require('../assets/background.jpg')
};


export default Images;