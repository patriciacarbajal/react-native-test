export default SignMap = [
  {
    name: 'Aquarius',
    dates: '1/20–2/18'
  },{
    name: 'Pisces',
    dates: '2/19–3/20'
  },{
    name: 'Aries',
    dates: '3/21–4/19'
  },{
    name: 'Taurus',
    dates: '4/20–5/20'
  },{
    name: 'Gemini',
    dates: '5/21–6/20'
  },{
    name: 'Cancer',
    dates: '6/21–7/22'
  },{
    name: 'Leo',
    dates: '7/23–8/22'
  },{
    name: 'Virgo',
    dates: '8/23–9/22'
  },{
    name: 'Libra',
    dates: '9/23–10/22'
  },{
    name: 'Scorpio',
    dates: '10/23–11/21'
  },{
    name: 'Sagittarius',
    dates: '11/22–12/21'
  },{
    name: 'Capricorn',
    dates: '12/22–1/19'
  }
];
