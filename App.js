import React, { Component } from 'react';
import SignGallery from './components/SignGallery'
import Horoscope from './components/Horoscope';
import SideMenu from './components/SideMenu';
import Terms from './components/Terms';
import Settings from './components/Settings';
import getSlideFromRightTransition from 'react-navigation-slide-from-right-transition';

import { View, Text } from "react-native";
import { createStackNavigator, createDrawerNavigator, DrawerActions, HeaderBackButton } from 'react-navigation';

export default class App extends Component<Props> {

  render() {
     return <RootStack/>
   }
}

const AppDrawer = createDrawerNavigator({
  SignGallery: {
    screen: SignGallery,
  }
}, {
  drawerWidth: 300,
  contentComponent: SideMenu
});



const RootStack = createStackNavigator(
  {  
    // Home: { screen: AppDrawer }, use drawer nav
    Home: { screen: SignGallery },
    SignGallery: {
      screen: SignGallery
    },
    Horoscope: {
      screen: Horoscope
    },
    Terms: {
      screen: Terms
    },
    Settings: {
      screen: Settings,
    }
  },
  {
    initialRouteName: 'Home',

    transitionConfig: getSlideFromRightTransition,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: true,
      title: 'AstroCall',
      headerStyle: {
        backgroundColor: '#673AB7',
        height: 80
      },
      headerTintColor: '#fff',
      headerMode: 'float',
      headerTransitionPreset: 'fade-in-place',
      headerTitleStyle: {
        fontSize: 26,
        fontWeight: '200',
      }
      // headerRight: <Text style={{ fontSize: 28, marginRight: 20 }} onPress={ () => navigation.dispatch(DrawerActions.toggleDrawer()) }>&#9776;</Text>  
    })
  }
);

